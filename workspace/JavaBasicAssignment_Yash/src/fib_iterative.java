import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * this is fib_iterative class.
 * @author drishti
 *
 */
public class fib_iterative {

	private static Logger logger = Logger.getLogger(fib_iterative.class.getName());
	/**
	 * this is our main function.
	 * @param args
	 */
	public static void main(String args[] )
	{
		try {
			FileHandler fh = new FileHandler("/home/drishti/workspace/JavaBasicAssignment_Yash/src/fib_iterative.txt");
			logger.addHandler(fh);
			logger.setLevel(Level.FINE);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			}
			catch(Exception e){
			System.out.println(e);
			}
		
			 //long a[]=new long[50];
			 int n=50; 
	     	long  a=0;
		 long   b=1,c;
			int i;
			//System.out.println(a[0]);
			//System.out.println(a[1]);
			logger.info(a+ " ");
			logger.info(b+" ");
			/**
			 * this for loop is used to print fibonacii series.
			 */
			for(i=2;i<n;i++)
			{
				c=a+b;
				a=b;
				b=c;
							
				logger.info(c+" ");
			}	
	}
	
}
