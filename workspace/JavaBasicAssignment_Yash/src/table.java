import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * this is a table class
 * @author drishti
 *
 */

public class table {
	
	private static Logger logger = Logger.getLogger(table.class.getName());

	/**
	 * this is a function to calculate the table and print t
	 * @param n
	 */
	
	static void fun(int n)
	{
		for(int i=1;i<=10;i++){
		  int x=n*i;
			logger.info(n + " * " + i + " = " + x);
		}
	}
	/**
	 * this is main class 
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			FileHandler fh = new FileHandler("/home/drishti/workspace/JavaBasicAssignment_Yash/src/table_logfile.txt");
			logger.addHandler(fh);
			logger.setLevel(Level.FINE);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			}
			catch(Exception e){
			System.out.println(e);
			}
	int n=3;
	fun(n);
	}
}
